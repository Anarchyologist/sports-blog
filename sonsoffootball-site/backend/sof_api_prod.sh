#!/bin/bash
NAME="backend"
DJANGODIR=/home/sonsoffootball/public_html/sports-blog/sonsoffootball-site/backend
SOCKFILE=/home/sonsoffootball/run/sonsoffootball.sock

USER=sonsoffootball
GROUP=sonsoffootball
NUM_WORKERS=9
TIMEOUT=240
MAX_REQUESTS=0
DJANGO_SETTINGS_MODULE=backend.settings
DJANGO_WSGI_MODULE=backend.wsgi
DJANGO_ASGI_MODULE=backend.asgi

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source /home/sonsoffootball/public_html/venvs/blog/bin/activate

export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn’t exist
RUNDIR=$(dirname $SOCKFILE)

# Remove previous socket if any
rm -f $SOCKFILE

# Start your Django Unicorn

# Programs meant to be run under supervisor should not daemonize themselves (do not use –daemon)
exec /home/sonsoffootball/public_html/venvs/blog/bin/gunicorn ${DJANGO_ASGI_MODULE}:application \
--name $NAME \
-k backend.reloaded.RestartableUvicornWorker \
-b unix:$SOCKFILE \
#--reload \
-u $USER \
-g $GROUP \
--workers $NUM_WORKERS \
--log-level error \
--log-file -

