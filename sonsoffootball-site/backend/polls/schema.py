import graphene
from graphene_django import DjangoObjectType
from graphql_auth import mutations
from graphene_django import DjangoListField
from .models import Article
from django.contrib.auth import get_user_model
#from graphql_auth.schema import UserQuery, MeQuery
import graphql_jwt
from graphene_django_cud.mutations import DjangoCreateMutation, DjangoDeleteMutation, DjangoUpdateMutation
from graphene_django_pagination import DjangoPaginationConnectionField
from graphql import GraphQLError

#    verify_account = mutations.VerifyAccount.Field()

#register = mutations.Register.Field()
#    update_account = mutations.UpdateAccount.Field()
#    resend_activation_email = mutations.ResendActivationEmail.Field()
#    send_password_reset_email = mutations.SendPasswordResetEmail.Field()
#    password_reset = mutations.PasswordReset.Field()

#class Query(UserQuery, MeQuery, graphene.ObjectType):
#    pass

#class Mutation(AuthMutation, graphene.ObjectType):
#    pass

class UserType(DjangoObjectType):
    class Meta:
        model = get_user_model()
        fields = ("id", "username", "password")

class ArticleType(DjangoObjectType):
    class Meta:
        model = Article
        fields = ("id", "title", "text", "date", "data", "photo")
        filter_fields = {"id": ["istartswith", "exact"]}
                

class Query(graphene.ObjectType):

    all_article = DjangoPaginationConnectionField(ArticleType)
    #all_article = DjangoListField(ArticleType)
    get_article = graphene.Field(ArticleType, id=graphene.String())
    all_user = DjangoListField(UserType)

    def resolve_all_article(root, info, **kwargs):
        return Article.objects.all()

    def resolve_get_article(root, info, id):
        return Article.objects.get(pk=id)

class CreateArticleMutation(DjangoCreateMutation):
    class Meta:
        model = Article

class UpdateArticleMutation(DjangoUpdateMutation):
    class Meta:
        model = Article

class DeleteArticleMutation(DjangoDeleteMutation):
    class Meta:
        model = Article

#class ObtainJSONWebToken(graphql_jwt.relay.JSONWebTokenMutation):
    #user = graphene.Field(UserType)

    #@classmethod
    #def resolve(cls, root, info, **kwargs):
    #    return cls(user=info.context.user)

#class ArticleMutation(graphene.Mutation):

    #class Arguments:
    #    title = graphene.String(required=True)

    #article = graphene.Field(ArticleType)

    #@classmethod
    #def mutate(cls, root, info, title):
    #    article = Article(title=title)
    #    article.save()
    #    return ArticleMutation(article=article)

class Mutation(graphene.ObjectType):

    #add_article = ArticleMutation.Field()
    add_article = CreateArticleMutation.Field()
    update_article = UpdateArticleMutation.Field()
    delete_article = DeleteArticleMutation.Field()
    token_auth = mutations.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()

schema = graphene.Schema(query=Query, mutation=Mutation)
