from django.urls import path
from graphene_django.views import GraphQLView
from polls.schema import schema
from django.views.decorators.csrf import csrf_exempt
from graphql_jwt.decorators import jwt_cookie


urlpatterns = [
        path("graphql", csrf_exempt(GraphQLView.as_view(graphiql=False, schema=schema))),
]
