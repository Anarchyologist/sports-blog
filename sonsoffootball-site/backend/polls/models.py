# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth import get_user_model
from graphene_django import DjangoObjectType
from graphene import relay
# Create your models here.

class Article(models.Model):
    title = models.CharField(max_length=254, null=True)
    text = models.TextField(null=True, blank=True)
    date = models.DateTimeField(null=True, blank=True)
    data = models.JSONField(null=True , blank=True)
    photo = models.TextField(null=True)

#class UserNode(DjangoObjectType):
#    class Meta:
#        model = User
#        interfaces = (relay.Node,)
#        only_fields = ('password', 'username')
