import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable , of , timer } from 'rxjs';
import { map , tap, catchError } from 'rxjs/operators';
import { Router, RouterModule, ActivatedRoute, ParamMap } from "@angular/router";
import {gql, Apollo} from 'apollo-angular';
import { get_articles } from './types';
import * as make from './jsonToHtml';
import {DomSanitizer} from "@angular/platform-browser";


@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    articles: any;
    all_art: any;
    carousel_art: any;
    images: any;
    p: number = 1;
    articleHTML: any = '';

    status: boolean = false;

    constructor(
        private apollo: Apollo,
        private router: Router,
        private sanitizer: DomSanitizer,
    ) {

        //this.articleHTML = this.sanitizer.bypassSecurityTrustHtml(this.articleHTML);
    }


    ngOnInit(): void {
        this.apollo.watchQuery({
            query: get_articles
        })
        .valueChanges
        .pipe(
            map( res => {
                this.articles = res.data;
                console.log(res.data);
                this.all_art = this.articles.allArticle.results;
                this.carousel_art = this.all_art.slice(0, 4);
                //console.log(this.carousel_art);
                //this.data = this.all_art.map((dat:any) =>{
                //    return JSON.parse(dat.data)
                //});
                //this.outputHtml(this.data);
                //console.log(this.articleHTML);
            }),
        ).subscribe();

    }


}

