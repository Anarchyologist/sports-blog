export function makeParagraph(obj:any){
    return `<p class="blog_post_text" style="text-align: justify; font-size:18px;">
                ${obj.data.text}
            </p>`
    }

export function makeImage(obj:any){
        const caption = obj.data.caption ? `<div class="blog_caption" style="font-style: italic; font-size: 0.9rem; margin-top:0.9rem;">
                                <p>${obj.data.caption}</p>
                            </div>` : ''
        return `<div class="blog_image" style="margin-bottom: 1rem;">
                                <img class="img-fluid" src="${obj.data.url}" alt="${obj.data.caption}"/>
                                ${caption}
                        </div>`


    }

export function makeHeader(obj:any){
        return `<h${obj.data.level} style="font-size: 22px;font-weight: bold;  border-left: 3px solid black; text-align: start; padding-left: 1rem; margin-top: 1rem; margin-bottom: 1rem;" class="blog_post_h${obj.data.level}">${obj.data.text}</h${obj.data.level}>`
    }

export function makeEmbed(obj:any) {

        const caption = obj.data.caption ? `<div class="list_item_btm_text">
            <p class="nws3_text1" style="font-style: italic; font-size: 0.9rem;"> ${obj.data.caption}</p>
        </div>` : ''
        return `<section class="nws3_sec4">
             <div class="row justify-content-center">
                 <div class="col-12 col-md-10 col-lg-8">

                     <div class="list_item_btm">
                             <div class="list_item_btm_img">
                             <iframe width="100%" height="415" src="${obj.data.embed}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                             </div>
                             ${caption}
                         </div>


                 </div>
             </div>
         </section>`
    }

export function  makeList(obj:any) {
        if (obj.data.style === 'unordered') {
            const list = obj.data.items.map((item:any) => {
                return `<li>${item}</li>`;
            });
            return `<ul class="blog_post_ul">
                            ${list.join('')}
                        </ul>`;

        } else {
            const list = obj.data.items.map((item:any) => {
                return `<li>${item}</li>`;
            });
            return `<ul class="blog_post_ul">
                            ${list.join('')}
                        </ul>`           
        }
    }

export function makeQuote(obj:any) {
        return `<div class="spcl_line mar_b30">
                        <blockquote>
                            <p class="spcl_line_p">
                                ${obj.data.text}
                            </p>
                        </blockquote>
                        <p>- ${obj.data.caption}</p>
                    </div>`

    }

export function makeWarning(obj:any) {
        return `<section class="nws3_sec4">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-lg-8">
                    <div class="table_warning">

                        <h3><span><i class="fas fa-exclamation"></i></span>${obj.data.title}</h3>
                        <p>${obj.data.message}</p>
                    </div>
                </div>
            </div>
        </section>	`
    }

export function makeChecklist(obj:any) {
        const list = obj.data.items.map((item:any) => {
            return `<div class="_1checkbox">
                <span class="_1checkbox_round"></span>
                ${item.text}</div>`;
        });
        return `<section class="nws3_sec4">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-lg-8">
                    <div class="table_top_sec">
                        ${list.join('')}
                    </div>
                </div>
            </div>
        </section>	`;

    }

export function makeDelimeter(obj:any) {
        return `<div class="ce-block">
            <div class="ce-block__content">
                <div class="ce-delimiter cdx-block"></div>
            </div>
            </div>\n`
    }

