import {gql, Apollo} from 'apollo-angular';

export const get_articles = gql`
    query all_articles {
        allArticle(ordering:"id,desc") {
            results{
                id
                date
                data
                title
                photo
            }
        }
    }`

export const get_article = gql`
    query get_article ($id:String){
        getArticle (id:$id){
            id
            data
            title
            date
            photo
        }
    }`

