import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable , of , timer } from 'rxjs';
import { map , tap, catchError } from 'rxjs/operators';
import { Router, RouterModule, ActivatedRoute, ParamMap } from "@angular/router";
import {gql, Apollo} from 'apollo-angular';
import { get_article } from '../types';
import * as make from '../jsonToHtml';
import {DomSanitizer, SafeResourceUrl, SafeUrl} from "@angular/platform-browser";
import { Title, Meta } from '@angular/platform-browser';



@Component({
    selector: 'app-article',
    templateUrl: './article.component.html',
    styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

    article: any;
    articledata: any;
    title: any;
    articleHTML: any = '';
    date: any;
    href: any;
    photo: any;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private apollo: Apollo,
        private sanitizer: DomSanitizer,
        private meta: Meta,
        private metatitle: Title,
    ) { }

    ngOnInit(): void {
        const id = this.route.snapshot.paramMap.get('id');

        this.href = window.location.href;
        this.apollo.watchQuery({
            query: get_article,
            variables: {
                id: id, 
            },
        })
        .valueChanges
        .pipe(
            map( (res:any) => {
                this.articledata = res.data;
                this.title = this.articledata.getArticle.title;
                this.photo = this.articledata.getArticle.photo;
                this.date = this.articledata.getArticle.date;
                this.article = JSON.parse(this.articledata.getArticle.data);
                this.metatitle.setTitle(this.title);
                this.meta.addTags([
                    {name: 'og:url', content: this.href},
                    {name: 'og:title', content: this.title},
                    {name: 'og:description', content: this.article[1].data.text },
                    {name: 'og:image', content: this.articledata.getArticle.photo},
                ]);
                //console.log(this.article);
                this.outputHtml(this.article);
                //this.articleHTML = this.sanitizer.bypassSecurityTrustHtml(this.articleHTML);
                //console.log(this.articleHTML);
            }),
        ).subscribe();

    }


    outputHtml(articleObj:any){
        articleObj.map((res:any)=>{
            switch (res.type) {
                case 'paragraph':
                    this.articleHTML += make.makeParagraph(res);
                    break;
                case 'simpleimage':
                    this.articleHTML += make.makeImage(res);
                    break;
                case 'header':
                    this.articleHTML += make.makeHeader(res);
                    break;
                case 'raw':
                    this.articleHTML += `<div class="ce-block">
                    <div class="ce-block__content">
                    <div class="ce-code">
                        <code>${res.data.html}</code>
                    </div>
                    </div>
                </div>\n`;
                    break;
                case 'list':
                    this.articleHTML += make.makeList(res);
                    break;
                case "quote":
                    this.articleHTML += make.makeQuote(res);
                    break;
                case "warning":
                    this.articleHTML += make.makeWarning(res);
                    break;
                case "checklist":
                    this.articleHTML += make.makeChecklist(res);
                    break;
                case "embed":
                    //console.log(res.data.embed);
                    //res.data.embed = this.sanitizer.bypassSecurityTrustResourceUrl(res.data.embed);
                    //console.log(res.data.embed);
                    this.articleHTML += make.makeEmbed(res);
                    break;
                case 'delimeter':
                    this.articleHTML += make.makeDelimeter(res);
                    break;
                default: {
                    break;
                }
            }
        })

    }


}
