import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './home/home.component';
import { SidebarComponent } from './sidebar/sidebar.component';

import { FacebookModule } from 'ngx-facebook';
import { ArticleComponent } from './home/article/article.component';
import { SafePipe } from './safe.pipe';
import {NgxPaginationModule} from 'ngx-pagination';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';


@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        SidebarComponent,
        ArticleComponent,
        SafePipe,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        GraphQLModule,
        HttpClientModule,
        NgbModule,
        NgxPaginationModule,
        ShareButtonsModule,
        ShareIconsModule,
        FacebookModule.forRoot(),
    ],
    providers: [
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
