import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { SidebarComponent } from './home/sidebar/sidebar.component';
import { AddarticleComponent } from './home/addarticle/addarticle.component';
import { EditarticleComponent } from './home/editarticle/editarticle.component';

import { httpInterceptProviders } from './http-interceptors';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth-guard.service';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GraphQLModule } from './graphql.module';
import EditorJS from '@editorjs/editorjs';
import { NgxEditorJSModule } from '@tinynodes/ngx-editorjs';
import { NgxEditorjsPluginsModule } from '@tinynodes/ngx-editorjs-plugins';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatFormFieldModule} from '@angular/material/form-field';
import { DatePipe } from '@angular/common'
import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        HomeComponent,
        SidebarComponent,
        AddarticleComponent,
        EditarticleComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        NgxPaginationModule,
        ReactiveFormsModule,
        NgbModule,
        GraphQLModule,
        MatFormFieldModule,
        NgxEditorjsPluginsModule,
        NgxEditorJSModule.forRoot({
            // Optional Configuration, see all keys below
            editorjs: {
                holder: 'editor',
                data: {
                    time: Date.now(),
                    version: EditorJS.version,
                    blocks: []
                }
            }
        }),
        BrowserAnimationsModule,
    ],
    providers: [
        AuthService,
        AuthGuard,
        httpInterceptProviders,
        DatePipe,
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
