import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AddarticleComponent } from './home/addarticle/addarticle.component';
import { EditarticleComponent } from './home/editarticle/editarticle.component';
import { AuthGuard } from './services/auth-guard.service';

const routes: Routes = [

    { path: 'login',
        component: LoginComponent,
    },
    { path: 'home',
        component: HomeComponent,
        canActivate: [AuthGuard],
    },
    { path: 'addarticle',
        component: AddarticleComponent,
        canActivate: [AuthGuard],
    },
    { path: 'edit/:id',
        component: EditarticleComponent,
        canActivate: [AuthGuard],
    },

    {path: '**', redirectTo: 'home', pathMatch: 'full'},
    {path: '', redirectTo: 'home', pathMatch: 'full'},
];

@NgModule({
imports: [RouterModule.forRoot(routes)],
exports: [RouterModule]
})
export class AppRoutingModule { }
