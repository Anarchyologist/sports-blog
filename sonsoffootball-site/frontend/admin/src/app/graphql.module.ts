import {NgModule} from '@angular/core';
import {APOLLO_OPTIONS} from 'apollo-angular';
import {ApolloClientOptions, InMemoryCache} from '@apollo/client/core';
import {HttpLink} from 'apollo-angular/http';
import { onError } from "@apollo/client/link/error";
import { HomeComponent } from './home/home.component';
import { Router, RouterModule, ActivatedRoute, ParamMap } from "@angular/router";

const uri = 'https://sonsoffootball.gr/graphql'; // <-- add the URL of the GraphQL server here

const link = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors)
        graphQLErrors.map(({ message, locations, path }) => {
            if (message=="Signature has expired"){
                localStorage.removeItem('accessToken');
                window.location.href = window.location.hostname + '/login';
                //this.AuthService.Logout;
            };
            /**console.log(
                `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
            );**/
        }
        );

    //if (networkError) console.log(`[Network error]: ${networkError}`);
});


export function createApollo(httpLink: HttpLink): ApolloClientOptions<any> {
    return {
        link: link.concat(httpLink.create({uri})),
        cache: new InMemoryCache({
             typePolicies: {
                Query: {
                    fields: {
                        allArticle: {
                            read( articles ) {
                                console.log(articles);
                                return articles;
                            },
                            merge( existing = {} , incoming ) {
                                console.log('incomming',incoming);
                                return incoming;
                            }
                        }
                    }
                },
                ArticleType: {
                    keyFields: ["id"],
                }
            }
        }),
    };
}

@NgModule({
    providers: [
        {
            provide: APOLLO_OPTIONS,
            useFactory: createApollo,
            deps: [HttpLink],
        },
    ],
})
export class GraphQLModule {}
