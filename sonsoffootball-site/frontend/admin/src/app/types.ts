import {gql, Apollo} from 'apollo-angular';

export const get_articles = gql`
    query all_articles( $ordering : String , $limit: Int , $offset: Int! ) {
        allArticle(ordering: $ordering , offset: $offset , limit: $limit) {
            results{
                id
                date
                data
                title
                photo
            }
        }
    }`

export const delete_article = gql`
    mutation delete_article ($id:ID!){
        deleteArticle (id:$id){
            found
            deletedId
        }
    }`

export const all_article = gql`
    query get_article ($id:String){
        getArticle (id:$id){
            id 
            data
            title
        }
    }`

export const update = gql`
    mutation updateArticle($id:ID!, $date:DateTime, $data:JSONString!, $title:String!, $photo:String){
        updateArticle(id:$id, input: {date:$date, data:$data, title:$title, photo:$photo}){
            article{
                id
                date
                data
                title
                photo
            }
        }
    }`

export const create = gql`
    mutation CreateArticle($date:DateTime, $data:JSONString!, $title:String!, $photo:String){
        addArticle(input: {date:$date, data:$data, title:$title, photo:$photo}){
            article{
                id
                date
                data
                title
                photo
            }
        }
    }`

export const refre = gql`
    mutation RefreshT($refreshToken: String!){
        refreshToken(refreshToken: $refreshToken){
            token
            payload
            refreshToken
        }
    }`

export const verify = gql`
    mutation VerifyT($token: String!){
        verifyToken(token: $token){
            payload
        }
    }`


