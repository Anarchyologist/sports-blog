import { Injectable } from "@angular/core";
import { AuthService } from '../services/auth.service';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse ,  HttpErrorResponse } from '@angular/common/http';
import { NavigationEnd, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of , throwError } from 'rxjs';
import { retry , map, catchError, mergeMap, tap } from 'rxjs/operators';
//import { debug } from 'util';
import {gql, Apollo} from 'apollo-angular';
//import { ErrorService } from '../services/error.service';
import { refre, verify } from './../types';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    refreshing : boolean = false;

    constructor(
        private auth: AuthService,
        private router: Router,
        private http: HttpClient,
        private apollo: Apollo,
        //private errorService: ErrorService,
    ) {}

    parseJwt (token:any) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));

        return JSON.parse(jsonPayload);

    }

    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>>{

        if(request.headers.get('skip')) {
            return next.handle(request);
        }

        let token = this.auth.getToken();
        if( token!="null" && token!=null ) {

            let exp = new Date(0);
            exp.setUTCSeconds(this.parseJwt(token).exp);
            const now = new Date( Date.now() );
            const diff = exp.getTime() - now.getTime() ;
            const minsLeft = Math.round(((diff % 86400000) % 3600000) / 60000);
            if(minsLeft > 0 && minsLeft < 15 && !this.refreshing && !request.headers.get('no_token_refresh')) {
                this.refreshing = true;
                //Refresh the fucking token
                let httpHeaders = new HttpHeaders();
                httpHeaders.set('Content-Type', 'application/json');
                httpHeaders = httpHeaders.append('skip', 'true');
                this.apollo.mutate({
                    mutation: refre,
                    variables: {
                        refreshToken: localStorage.getItem('refreshToken'), 
                    },
                }).pipe().subscribe( res => {
                    let tk:any = res.data
                    localStorage.setItem('accessToken', tk.refreshToken.token);
                    localStorage.setItem('refreshToken', tk.refreshToken.refreshToken);
                    this.refreshing = false;
                });
            }

            request  = request.clone({
                setHeaders: { Authorization: `JWT ${this.auth.getToken()}` }
            });
        }
        return next.handle(request).pipe(
            //retry(1),
            tap(
                event => {
                    //const err:any = event;
                    //console.log(err.body);
                    //let error:any = err.body;
                    if (event instanceof HttpResponse) {
                    }
                },
                /**error => {
                    if(error.status == 401) {
                        let httpHeaders = new HttpHeaders();
                        httpHeaders.set('Content-Type', 'application/json');
                        httpHeaders = httpHeaders.append('skip', 'true');
                        this.apollo.mutate({
                            mutation: verify,
                            variables: {
                                token: localStorage.getItem('accessToken'),
                            },
                        }).pipe().subscribe(
                            res => {
                                console.log(res);
                            },
                            err => {

                                console.log(err);
                                if( err.error.non_field_errors) {
                                    if(
                                        err.error.non_field_errors[0] ===  'Error decoding signature.' ||
                                        err.error.non_field_errors[0] ===  'Signature has expired.'
                                    ) {
                                        localStorage.removeItem('accessToken');
                                        this.router.navigate(['/login']);
                                    }
                                }
                                return err;
                            }
                        );
                    }
                }**/
            )
        )
    }
}
