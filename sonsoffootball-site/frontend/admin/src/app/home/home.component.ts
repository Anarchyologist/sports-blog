import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable , of , timer } from 'rxjs';
import { map , tap, catchError } from 'rxjs/operators';
import { Router, RouterModule, ActivatedRoute, ParamMap } from "@angular/router";
import {gql, Apollo} from 'apollo-angular';
import { get_articles, delete_article } from './../types';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    p: number = 1;
    articles: any;
    all_art: any ;
    data: any;
    images: any;
    artQuery: any;

    constructor(
        private apollo: Apollo,
        private router: Router,
    ) { }

    ngOnInit(): void {
        let vars = {};
        vars = { 
            ordering: 'id,desc' , 
            offset: 0,
            ...vars 
        };

        vars = {
            limit: 20,
            ...vars
        }
        this.artQuery = this.apollo.watchQuery({
            query: get_articles,
            variables: vars,
            //fetchPolicy: 'network-only',
        });
        this.fetchData();
    }

    DeleteArticle(article_id: number){
        /**console.log(article_id);
        var index:number = this.all_art.findIndex((obj:any) => obj.id == article_id);
        console.log(index);
        console.log(Object.isFrozen(this.all_art[index]));
        console.log(this.all_art);
         **/
        this.apollo.mutate({
            mutation: delete_article,
            variables: {
                id: article_id,
            },
            refetchQueries: [
                get_articles
            ],
        }).subscribe(
        );

    }

    fetchData(){

        this.artQuery.valueChanges.pipe(
                tap( res => console.log(res)),
        ).subscribe(
            ({data}:any) => 
            {
                if(data.allArticle) {
                    this.all_art = data.allArticle.results;
                }
                console.log(this.all_art);
            },
            (err:any) => {
                console.log('got error' , err);
            }

        );

    }


}
