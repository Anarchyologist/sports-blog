import { Component, OnInit } from '@angular/core';
import { NgxEditorJSService } from '@tinynodes/ngx-editorjs';
import EditorJs from '@editorjs/editorjs';
import { Router, RouterModule, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import {gql, Apollo} from 'apollo-angular';
import { map } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { all_article, update } from './../../types';


@Component({
    selector: 'app-editarticle',
    templateUrl: './editarticle.component.html',
    styleUrls: ['./editarticle.component.css']
})
export class EditarticleComponent implements OnInit {

    editor:any;
    articledata: any;
    title: any;
    image: any;

    constructor(
        private readonly editorService: NgxEditorJSService,
        private apollo: Apollo,
        private router:Router,
        private datepipe: DatePipe,
        private route: ActivatedRoute
    ) { }

    ngOnInit(): void {
        const id = this.route.snapshot.paramMap.get('id');
        this.apollo.watchQuery({
            query: all_article,
            variables: {
                id: id,
            },
        })
        .valueChanges
        .pipe(
            map( res => {
                this.articledata = res.data;
                this.title = this.articledata.getArticle.title;
                //console.log(this.articledata.getArticle);
                //console.log(JSON.parse(this.articledata.getArticle.data));
                this.editorService
                    .update({
                        holder: this.editor,
                        data: {
                            blocks:JSON.parse(this.articledata.getArticle.data),
                        },
                    })
                    .subscribe();
            }),
        ).subscribe();

    }

    onSave() {
        const id = this.route.snapshot.paramMap.get('id');
        this.editorService
            .save({holder: this.editor})
            .subscribe(dat=>{
                //console.log(dat.result);
                this.image = dat.result.blocks.find(res => res.type=="simpleimage");
                //console.log(this.image.data.url);
                let datetime:any  = this.datepipe.transform(dat.result.time, 'yyyy-MM-dd');
                let time = new Date(datetime)
                this.apollo.mutate({
                    mutation: update,
                    variables: {
                        id: id,
                        date: time,
                        data: JSON.stringify(dat.result.blocks),
                        title: this.title,
                        photo: this.image.data.url,
                    },
                }).subscribe(res=>{
                    this.router.navigate(['/home']);
                });
            });
    }

}
