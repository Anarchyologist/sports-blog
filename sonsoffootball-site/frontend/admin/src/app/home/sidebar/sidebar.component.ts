import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from "@angular/router";

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

    constructor(
        private router:Router,
    ) { }

    ngOnInit(): void {
    }
    
    status: boolean = false;

    clickEvent(){
        this.status = !this.status;       
    }

    Logout() {
        localStorage.removeItem('accessToken');
        this.router.navigate(['login']);
    }
}
