import { Component, OnInit, ViewChild } from '@angular/core';
import EditorJs from '@editorjs/editorjs';
import Header from '@editorjs/header';
import { FormControl } from '@angular/forms';
import { NgxEditorJSService } from '@tinynodes/ngx-editorjs';
import {gql, Apollo} from 'apollo-angular';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { create, get_articles } from './../../types';
import cloneDeep from 'lodash/cloneDeep';

@Component({
    selector: 'app-addarticle',
    templateUrl: './addarticle.component.html',
    styleUrls: ['./addarticle.component.css']
})
export class AddarticleComponent implements OnInit {

    editor: any;
    title: any;
    image: any;

    constructor(
        private readonly editorService: NgxEditorJSService,
        private apollo: Apollo,
        private datepipe: DatePipe,
        private router:Router,
    ) { }

    ngOnInit(): void {
        /**this.editor = new EditorJs( {
            holder: 'editorjs',
            tools: {
                header: Header,
            }
        });**/
    }

    onSave() {
        this.editorService
            .save({holder: this.editor})
            .subscribe(dat=>{
                //console.log(dat.result);
                this.image = dat.result.blocks.find(res => res.type=="simpleimage");
                //console.log(this.image.data.url);
                let datetime:any  = this.datepipe.transform(dat.result.time, 'yyyy-MM-dd');
                let time = new Date(datetime)
                this.apollo.mutate({
                    mutation: create,
                    variables: {
                        date: time,
                        data: JSON.stringify(dat.result.blocks),
                        title: this.title,
                        photo: this.image.data.url,
                    },
                    update: ( store , { data: { addArticle }} :any ) => {
                        const data :any = cloneDeep(store.readQuery({query: get_articles }));
                        //data.allUsers.totalCount++;
                        data.allArticle.results = [ addArticle.article, ...data.allArticle.results ];
                        store.writeQuery( { query: get_articles , data });
                    }
                }).subscribe(res=>{
                    this.router.navigate(['/home']);
                });
            });
    }

}
