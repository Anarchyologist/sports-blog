import { Component, OnInit } from '@angular/core';
import {gql, Apollo} from 'apollo-angular';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { Observable} from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

const login = gql`
    mutation SignIn($password: String!, $username: String!){
        tokenAuth (password: $password, username: $username){
            token
            success
            errors
            refreshToken
        }
    }`

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    public Username: string = '';
    public Passw: string = '';
    token: any;

    constructor(
        private apollo:Apollo,
        private router:Router,       
    ) { }

    ngOnInit(): void {
        if (localStorage.getItem('accessToken')!="null")
                this.router.navigate(['/home']);
    }

    log_in(){
        this.apollo.mutate({
            mutation: login,
            variables: {
                password: this.Passw,
                username: this.Username,
            },
        }).pipe(
            map( res => {
                this.token = res.data;
                //console.log(this.token.tokenAuth.errors.nonFieldErrors);
                localStorage.setItem('accessToken', this.token.tokenAuth.token);
                localStorage.setItem('refreshToken', this.token.tokenAuth.refreshToken);
                if (localStorage.getItem('accessToken')!="null")
                    this.router.navigate(['/home']);
            })
        ).subscribe();
    }



}
