import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable , of } from 'rxjs';
import { map , tap, catchError } from 'rxjs/operators';
import { Router } from "@angular/router";
import {gql, Apollo} from 'apollo-angular';
import { verify } from './../types';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    public getToken(): string | null {
        return localStorage.getItem('accessToken');
    }

    constructor(
        private http:HttpClient,
        private router:Router,
        private apollo:Apollo,
    ) { }

    public isAuthenticated(): Observable<boolean> {
        const token = this.getToken();
        if(!token) {
            return of(false);
        }
        return this.apollo.mutate({
            mutation: verify,
            variables: {
                token: localStorage.getItem('accessToken'),
            },
        }).pipe(
            map( res => {
                let vtoken:any = res.data;
                if (vtoken.verifyToken=="null")
                    return false;
                return true;
            }),
            catchError(err => {
                return of(false);
            }),
        );

    }


}
