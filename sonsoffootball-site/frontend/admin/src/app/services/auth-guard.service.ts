import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { Observable , of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate
{
    constructor(
        private auth: AuthService,
        private router: Router
    ) {}

    canActivate(): Observable<boolean> | Promise<boolean>| boolean
    {
        var k = this.auth.isAuthenticated();
        k.pipe(
            tap( res => {
                if(!res) {
                    this.router.navigate(['/login']);
                }
            }),
            catchError(err => {
                return of(false);
            }),
        ).subscribe();
        return k;
    }
}
